import argparse
import os
from parser import Parser
from typing import List

import numpy as np

from Community import Community
from community_finder import CommunityFinder
from constants import NUMBER_PARTITIONS
from plotter_utils import make_gif_across_community_resolutions, plot_heatmap
from write_results_utils import write_community_coordinates


def handle_options():

    parser = argparse.ArgumentParser()
    parser.add_argument('--counts', type=str, nargs='?',
                        help='Path to Hi-C counts file', default='example_files/example.counts')
    parser.add_argument('--bed', type=str, nargs='?',
                        help='Path to Hi-C bed file.', default='example_files/example.bed')

    OPTIONS = parser.parse_args()

    return OPTIONS


def main():

    OPTIONS = handle_options()
    counts_parser = Parser(OPTIONS.counts, OPTIONS.bed)
    bin_map = counts_parser.parse_bed()
    counts_arrays = counts_parser.parse_counts(bin_map)

    gammas = [0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8,
              1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0]

    plot_dir = 'output/plots/'
    if not os.path.exists(plot_dir):
        os.makedirs(plot_dir)

    boundaries_dir = 'output/boundary_files/'
    if not os.path.exists(boundaries_dir):
        os.makedirs(boundaries_dir)

    # Iterate over Hi-C genomic regions
    for region in counts_arrays:
        counts = counts_arrays.get(region)
        regional_bin_map = bin_map.get(region)
        community_finder = CommunityFinder(counts, regional_bin_map)
        # Iterate over resolutions for partitioning the network
        for gamma in gammas:
            print(
                f'finding communities for region {region} with resolution {gamma}')
            community_coordinates = community_finder.find_communities(gamma)
            community_file = f"{boundaries_dir}Boundaries_{region}_{gamma}.txt"
            write_community_coordinates(community_coordinates, community_file)
            # Plot heatmaps of Hi-C data with community boundaries drawn
            heatmap_fname = f"{plot_dir}{region}_{gamma}.png"
            plot_heatmap(counts, heatmap_fname,
                         communities=community_coordinates, bin_map=regional_bin_map)

        make_gif_across_community_resolutions(gammas, region, plot_dir)


if __name__ == "__main__":
    main()
