from typing import List

from Community import Community


def write_community_coordinates(coordinates: List[Community], fname: str):
    out_file = open(fname, 'w')
    header = "#Chrom, Start, Stop\n"
    out_file.write(header)
    for coord in coordinates:
        line = f"{coord.chrom}\t{coord.start}\t{coord.end}\n"
        out_file.write(line)

    out_file.close()
